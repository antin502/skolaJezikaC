﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.ModelSpajanja
{
    class UcenikNaKurs : ICloneable
    {
        public int Id { get; set; }
        public Ucenik ucenik { get; set; }
        public Kurs.Kurs kurs { get; set; }
        public Aktivnost aktivnost { get; set; }
        public UcenikNaKurs() { }
        public UcenikNaKurs(Ucenik u, Kurs.Kurs k,Aktivnost a)
        {
            this.ucenik = u;
            this.kurs = k;
            this.aktivnost = a;
        }

        public object Clone()
        {
            UcenikNaKurs original = new UcenikNaKurs();
            original.ucenik = ucenik;
            original.kurs = kurs;
            original.aktivnost = aktivnost;
            return original;
        }
    }
}
