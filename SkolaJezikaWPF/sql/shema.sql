--string naziv, string adresa, string telefon, string email, string internetStranica,
--                        int pib, int mBroj, int zRacun
-- drop database SkolaJezikaSF57

create database SkolaJezikaSF57
use SkolaJezikaSF57

create table Skola(
	Naziv		varchar(30)		not null,
	Adresa		varchar(30)		not null,
	Telefon		varchar(15)		not null,
	Email		varchar(20)		not null,
	Sajt		varchar(15)		not null,
	PIB			integer			not null,
	MB			varchar(8)		not null,
	ZR			varchar(14)		not null
)

insert into Skola
	values('ime skole', 'adresa skole', '060606106016', 'skola@skola.rs', 'skola.rs', 121212112, '01212121', '111-1111111-11')

select * from Skola

--string username, string password, string ime,string prezime, string jmbg, Aktivnost a, Admin admin

create table Korisnik(
	kIme		varchar(25)	not null,
	kLoz		varchar(18)	not null,
	ime			varchar(20)	not null,
	prezime		varchar(15)	not null,
	jmbg		varchar(13)	not null,
	aktivnost	bit		not null,
	admin		bit		not null,
	primary key(kIme,jmbg)
)

insert into Korisnik
	values('user','pass','user','prezime','5555555555555',0,1)
insert into Korisnik
	values('darko','roks','darko','roljic','6666666666666',0,1)
insert into Korisnik
	values('ime1','pass2','ime2','prezime2','5222222222222',0,0)
insert into Korisnik
	values('1','1','pera','peric','8888888888888',0,0)
insert into Korisnik
	values('v','v','v','v','9999999999999',0,1)
insert into Korisnik
	values('kor','kor','kor','kor','1234785461203',0,0)

	select * from Korisnik

	

--string ime, string prezime, string jmbg, double plata, Aktivnost aktivan

create table Nastavnik(
	ime			varchar(20)	not null,
	prezime		varchar(15)	not null,
	jmbg		varchar(13)	primary key,
	plata		money		not null,
	aktivnost	bit		not null
)


insert into Nastavnik
	values('Dragan','Dragovic','1291034810400',10050,0)
insert into Nastavnik
	values('Natasa','Nastasic','1390391051000',9225,0)
insert into Nastavnik
	values('Nemanja','Jovanovic','0214104010001',9910,0)

insert into Nastavnik
	values('Mladen','Vi�kovi�','7847412357891',9910,0)
insert into Nastavnik
	values('Olgica','Marjanovi�','7820314591025',9910,0)
insert into Nastavnik
	values('Sonja','Avramovi�','0047812036971',9910,0)
insert into Nastavnik
	values('Dragan','Ferenc','7840069712348',9910,0)
insert into Nastavnik
	values('Maja','Peri�','5477701248902',9910,0)
insert into Nastavnik
	values('Ognjen','Adamovic','1127801469875',9910,0)
insert into Nastavnik
	values('Miroslav','Mirovi�','1522717486905',9910,0)
insert into Nastavnik
	values('Vladimir','Milanovi�','0712894352688',9910,0)


select * from Nastavnik where aktivnost!=1


--string ime, string prezime, string jmbg, Aktivnost aktivan

create table Ucenik(
	ime			varchar(20)	not null,
	prezime		varchar(15)	not null,
	jmbg		varchar(13)	primary key,
	aktivnost	bit		not null
)

insert into Ucenik
	values('Pera','Peric','0912194319999',0)

insert into Ucenik
	values('Dobrivoje','Juric','1085014114411',0)

insert into Ucenik
	values('Djuro','Beri�','1085014114412',0)

insert into Ucenik
	values('Mladen','Popovi�','5753432028275',0)
insert into Ucenik
	values('Uro�','Nikoli�','9992148092006',0)
insert into Ucenik
	values('Stefan','Glumac','0073147886468',0)
insert into Ucenik
	values('Lazar','Kova�evi�','4124693601948',0)
insert into Ucenik
	values('Nemanja','Matija�','2906667284068',0)
insert into Ucenik
	values('Igor','Jakovljevi�','0334440233595',0)
insert into Ucenik
	values('Sla�ana','Mandi�','0897198663148',0)
insert into Ucenik
	values('Marija','Kostov','8436627186402',0)
insert into Ucenik
	values('Maja','Todorovi�','6437649084268',0)
insert into Ucenik
	values('Marko','Jovanovi�','0550334522053',0)
insert into Ucenik
	values('Rade','Andri�','0297558895951',0)
insert into Ucenik
	values('Darko','Mari�','8726889901022',0)
insert into Ucenik
	values('Ivan','Petrovi�','7377133067878',0)
insert into Ucenik
	values('Ivana','Risti�','5532020665663',0)
insert into Ucenik
	values('Aleksandra','Luki�','9000699751168',0)
insert into Ucenik
	values('Tijana','Mati�','3521785409489',0)
insert into Ucenik
	values('Jelena','Mijatovi�','3437097591704',0)
insert into Ucenik
	values('David','Stankovi�','6415875054823',0)
insert into Ucenik
	values('Nikola','Baji�','1129032199375',0)
insert into Ucenik
	values('Vladimir','Fazlonovi�','0348775612272',0)


select * from Ucenik where aktivnost!=1


--int oznaka, string naziv,Aktivnost aktivnost

create table Jezik(
	oznaka		integer	primary key identity,
	naziv		varchar(20) not null,
	aktivnost	bit		not null
)

insert into Jezik
	values('Engleski 2',0)
insert into Jezik
	values('Nemacki 1',0)
insert into Jezik
	values('Engleski 1',0)
insert into Jezik
	values('Nemacki 2',0)
insert into Jezik
	values('Spanski 2',0)
insert into Jezik
	values('Tadzikistanski 1',1)
insert into Jezik
	values('Spanski 2',0)
insert into Jezik
	values('Ruski 2',0)

select * from Jezik where aktivnost!=1



--int id,string name,Aktivnost aktivnost


create table Tip(
	ID		integer	primary key identity,
	naziv		varchar(20) not null,
	aktivnost	bit		not null
)

insert into Tip
	values('c1',0)
insert into Tip
	values('c2',0)
insert into Tip
	values('c3',0)
insert into Tip
	values('c4',0)
insert into Tip
	values('c5',0)
insert into Tip
	values('c6',0)

select * from Tip

--int id, Jezik jezik,TipKursa tip, DateTime d_poc, DateTime d_kra, int cena,Nastavnik n,Aktivnost aktivnost

create table Kurs(
	ID				integer		primary key		identity,
	OznakaJezika	integer	foreign key	references Jezik(oznaka) not null,
	OznakaTipa		integer	foreign key references Tip(ID) not null,
	datum_pocetka	datetime	not null,
	datum_kraja		datetime	not null,
	cena			money		not null,
	jmbgNastavnika	varchar(13)	foreign key references Nastavnik(jmbg) not null,
	aktivnost	bit		not null
)

insert into Kurs
	values(1,1,'2016-12-06','2016-12-07',900,'1291034810400',0)

insert into Kurs
	values(2,3,'2016-12-07','2016-12-16',2000,'1291034810400',0)

insert into Kurs
	values(1,1,'2017-12-07','2017-12-16',2000,'0047812036971',0)
insert into Kurs
	values(2,6,'2017-01-07','2017-01-16',2000,'0214104010001',0)
insert into Kurs
	values(3,5,'2017-01-17','2017-02-07',2000,'1522717486905',0)
insert into Kurs
	values(5,4,'2017-02-07','2017-03-02',2000,'5477701248902',0)
insert into Kurs
	values(8,5,'2017-01-15','2017-02-15',2000,'7840069712348',0)
insert into Kurs
	values(8,5,'2017-03-15','2017-06-15',5000,'5477701248902',0)

select * from Kurs
select * from Ucenik




--Ucenik u, Kurs.Kurs k,Aktivnost a

create table UcenikNaKursu(
	ID				integer		primary key		identity,
	OznakaKursa		integer		foreign key references	Kurs(ID) not null,
	jmbgUcenika		varchar(13)	foreign key	references	Ucenik(jmbg) not null,
	aktivnost	bit		not null
)

insert into UcenikNaKursu
	values(1,'0912194319999',0)
insert into UcenikNaKursu
	values(1,'1085014114412',0)

insert into UcenikNaKursu
	values(7,'7377133067878',0)
insert into UcenikNaKursu
	values(7,'8436627186402',0)
insert into UcenikNaKursu
	values(7,'8726889901022',0)
insert into UcenikNaKursu
	values(7,'5532020665663',0)
insert into UcenikNaKursu
	values(8,'2906667284068',0)
insert into UcenikNaKursu
	values(8,'3521785409489',0)

select * from UcenikNaKursu

--int id,Ucenik u,Kurs k,DateTime datum,long cena,Aktivnost ak

create table Uplata(
	ID				integer		primary key		identity,
	jmbgUcenika		varchar(13)	foreign key	references	Ucenik(jmbg) not null,
	OznakaKursa		integer		foreign key references	Kurs(ID) not null,
	datum_uplate	datetime	not null,
	cena			money	not null,
	aktivnost	bit		not null
)

insert into Uplata
	values('0912194319999',1,'2016-12-09',800,0)

insert into Uplata
	values('7377133067878',7,'2016-12-25',600,0)
insert into Uplata
	values('8726889901022',7,'2016-12-25',600,0)
insert into Uplata
	values('2906667284068',8,'2016-12-25',600,0)

select * from Uplata


