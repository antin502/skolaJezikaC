﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SkolaJezikaWPF.Validacije
{
    class JmbgTeacherValidation: ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            String v = value as string;
            if (v != null && Aplikacija.Instanca.PronadjiNastavnika(v) == null && v.Length == 13)
                return new ValidationResult(true, null);
            else
                return new ValidationResult(false, "Niste uneli sva polja");
        }

    }
}
