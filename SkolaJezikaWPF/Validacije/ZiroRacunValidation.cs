﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SkolaJezikaWPF.Validacije
{
    class ZiroRacunValidation:ValidationRule
    {
        Regex regex = new Regex(@"\b\d{3}-\d{7}-\d{2}\b", RegexOptions.IgnoreCase);

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            String v = value as string;
            if (v != null && regex.Match(v).Success)
                return new ValidationResult(true, null);
            else
                return new ValidationResult(false, "Neispravan format broja telefona");
        }

    }
}
