﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SkolaJezikaWPF.Validacije
{
    class UsernameValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            String v = value as string;
            if (v != null && Aplikacija.Instanca.PronadjiKorisnikaPoUsername(v)==null)
                return new ValidationResult(true, null);
            else
                return new ValidationResult(false, "Korisničko nije uneto ili postoji u bazi");
        }

    }
}
