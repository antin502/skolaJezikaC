﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SkolaJezikaWPF.Validacije
{
    class EmptyValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            String v = value as string;
            if (v != null && v.Trim() != "")
                return new ValidationResult(true, null);
            else
                return new ValidationResult(false, "Niste uneli sva polja");
        }

    }
}
