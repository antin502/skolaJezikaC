﻿using SkolaJezikaWPF.GUI;
using SkolaJezikaWPF.GUI.FormeDodavanje;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Menadzeri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SkolaJezikaWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Title = "Login";
            Username.Focus();
        }
        private void osveziBox()
        {
            Username.Text = "";
            Password.Password = "";
            JmbgNastavnik.Text = "";
            JmbgUcenik.Text = "";
            Username.Focus();
        }

        private void login_click(object sender, RoutedEventArgs e)
        {
            string username = Username.Text;
            string password = Password.Password;
            Korisnik k = DAO.KorisnikDAO.Login(username, password);
            if (k == null)
            {
                MessageBox.Show("Pogrešni login podaci", "Greška");
            }
            else
            {if (k.Admin == Admin.Jeste && k.Aktivnost == Aktivnost.Aktivan)
                {
                    this.Hide();
                    AdminMenu am = new AdminMenu();
                    am.ShowDialog();
                    this.Show();
                    osveziBox();
                } else if (k.Admin == Admin.Nije && k.Aktivnost == Aktivnost.Aktivan)
                {
                    this.Hide();
                    GlavniMeni gm = new GlavniMeni();
                    gm.ShowDialog();
                    this.Show();
                    osveziBox();
                }
                else
                {
                    MessageBox.Show("Pogrešni login podaci", "Greška");
                }
            }
        }
        private void Jmbg_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Utils.CheckIsNumeric(e);
        }

        private void nastavnik_Click(object sender, RoutedEventArgs e)
        {
            string jmbg = JmbgNastavnik.Text;
            Aplikacija.Instanca.ucitajNastavnike("");
            Nastavnik n = Aplikacija.Instanca.PronadjiNastavnika(jmbg);
            if (n == null || n.Aktivnost==Aktivnost.NeAktivan)
            {
                MessageBox.Show("Pogrešni login podaci", "Greška");
            }
            else
            {
                this.Hide();
                DodavanjeIizmenaNastavnika dn = new DodavanjeIizmenaNastavnika(false, n, true);
                dn.ShowDialog();
                this.Show();
                osveziBox();
            }
        }

        private void ucenik_Click(object sender, RoutedEventArgs e)
        {
            string jmbg = JmbgUcenik.Text;
            Aplikacija.Instanca.ucitajUcenike("");
            Ucenik u = Aplikacija.Instanca.PronadjiUcenika(jmbg);
            if (u == null)
            {
                MessageBox.Show("Pogrešni login podaci", "Greška");
            }
            else
            {
                this.Hide();
                DodavanjeIizmenaUcenika du = new DodavanjeIizmenaUcenika(false, u, true);
                du.ShowDialog();
                this.Show();
                osveziBox();
            }
        }
    }
}
