﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Kurs
{
    public class Uplata
    {
        public int Id { get; set; }
        public Ucenik Uplatilac { get; set; }
        public Kurs Kurs { get; set; }
        public DateTime DatumUplate { get; set; }
        public double Cena { get; set; }
        public Aktivnost Aktivnost { get; set; }
        public Uplata()
        {
            this.Id = 0;
            this.Cena = 0;
            this.Aktivnost = Aktivnost.Aktivan;
            this.DatumUplate = DateTime.Now;
        }
        public Uplata(Ucenik u,Kurs k,DateTime datum,double cena,Aktivnost ak)
        {
            this.Uplatilac = u;
            this.Kurs = k;
            this.DatumUplate = datum;
            this.Cena = cena;
            this.Aktivnost = ak;
        }
    }
}
