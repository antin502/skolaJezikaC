﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Kurs
{
    public class Jezik:ICloneable
    {
        public int Oznaka { get; set; }
        public string Naziv { get; set; }
        public Aktivnost Aktivnost { get; set; }
        public Jezik()
        {
            this.Oznaka = 0;
            this.Naziv = "";
            this.Aktivnost = Aktivnost.Aktivan;
        }
        public Jezik(int oznaka, string naziv,Aktivnost aktivnost)
        {
            this.Oznaka = oznaka;
            this.Naziv = naziv;
            this.Aktivnost = aktivnost;
        }
        public override string ToString()
        {
            return this.Naziv;
        }

        public object Clone()
        {
            Jezik original = new Jezik();
            original.Oznaka = Oznaka;
            original.Naziv = Naziv;
            original.Aktivnost = Aktivnost;
            return original;
        }
    }
}
