﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Kurs
{
    public class Kurs : ICloneable
    {
        public int OznakaKursa { get; set; }
        public Jezik StraniJezik { get; set; }
        public TipKursa NivoKursa { get; set; }
        public DateTime DatePocetak { get; set; }
        public DateTime DateKraj { get; set; }
        public double Cena { get; set; }
        public Nastavnik Predavac { get; set; }
        public Aktivnost Aktivnost { get; set; }
        public Kurs(){
            this.OznakaKursa = 0;
            this.DatePocetak = DateTime.Today;
            this.DateKraj = DateTime.Today;
            this.Cena = 0;
            this.Aktivnost = Aktivnost.Aktivan;
        }
        public Kurs (int id, Jezik jezik,TipKursa tip, DateTime d_poc, DateTime d_kra, double cena,Nastavnik n,Aktivnost aktivnost)
        {
            this.OznakaKursa = id;
            this.StraniJezik = jezik;
            this.NivoKursa = tip;
            this.DatePocetak = d_poc;
            this.DateKraj = d_kra;
            this.Cena = cena;
            this.Predavac = n;
            this.Aktivnost = aktivnost;
        }
        public override string ToString()
        {
            return StraniJezik.Naziv+"   "+NivoKursa.Naziv;
        }
        public object Clone()
        {
            Kurs original = new Kurs();
            original.OznakaKursa = OznakaKursa;
            original.StraniJezik = StraniJezik;
            original.NivoKursa = NivoKursa;
            original.DatePocetak = DatePocetak;
            original.DateKraj = DateKraj;
            original.Cena = Cena;
            original.Predavac = Predavac;
            original.Aktivnost = Aktivnost;
            return original;
        }
    }
}
