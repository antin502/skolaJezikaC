﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Kurs
{
    public class TipKursa:ICloneable
    {
        public int ID { get; set; }
        public string Naziv { get; set; }
        public Aktivnost Aktivnost { get; set; }
        public TipKursa()
        {
            this.ID = 0;
            this.Naziv = "";
            this.Aktivnost = Aktivnost.Aktivan;
        }
        public TipKursa(int id,string name,Aktivnost aktivnost)
        {
            this.ID = id;
            this.Naziv = name;
            this.Aktivnost = aktivnost;
        }

        public object Clone()
        {
            TipKursa original = new TipKursa();
            original.ID = ID;
            original.Naziv = Naziv;
            original.Aktivnost = Aktivnost;
            return original;
        }
        public override string ToString()
        {
            return Naziv;
        }
    }
}
