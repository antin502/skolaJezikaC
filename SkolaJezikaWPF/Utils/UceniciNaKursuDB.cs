﻿using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Menadzeri
{
    class UceniciNaKursuDB
    {
        public static ObservableCollection<UcenikNaKurs> dodati;
        public static ObservableCollection<UcenikNaKurs> obrisani;

        public static void initValues()
        {
            dodati = new ObservableCollection<UcenikNaKurs>();
            obrisani = new ObservableCollection<UcenikNaKurs>();
        }

    }
}
