﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SkolaJezikaWPF.Menadzeri
{
    class Utils
    {
        public static void CheckIsNumeric(TextCompositionEventArgs e)
        {
            long result;

            if (!(long.TryParse(e.Text, out result) || e.Text == "."))
            {
                e.Handled = true;
            }
        }

    }
}
