﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.GUI.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AdminMenu : Window
    {
        Skola skola = SkolaDAO.Read();
        public AdminMenu()
        {
            InitializeComponent();
            this.DataContext = skola;
            nameSchool.Text = skola.Naziv;
        }


        private void gotoKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciMenu km = new KorisniciMenu();
            km.ShowDialog();
        }

        private void NastavniciMenu_Click(object sender, RoutedEventArgs e)
        {
            NastavniciMenu nm = new NastavniciMenu();
            nm.ShowDialog();
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(Validation.GetHasError(phoneSchool) || Validation.GetHasError(emailSchool) || Validation.GetHasError(zrSchool) || Validation.GetHasError(internetSchool) ||
                Validation.GetHasError(nameSchool) || Validation.GetHasError(streetSchool)){}
            else
            {
                SkolaDAO.Update(skola);
                MessageBox.Show("Uspešno izmenjeni podaci o školi");
            }
        }
    }
}
