﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.GUI.FormeDodavanje;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Menadzeri;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.Admin
{
    /// <summary>
    /// Interaction logic for KorisniciMenu.xaml
    /// </summary>
    public partial class KorisniciMenu : Window
    {
        CollectionViewSource cvs;
        public KorisniciMenu()
        {
            InitializeComponent();
            sviKorisnici.CanUserReorderColumns = false;
            sviKorisnici.IsReadOnly = true;
            sviKorisnici.SelectionMode = DataGridSelectionMode.Single;
            sviKorisnici.SelectionUnit = DataGridSelectionUnit.FullRow;
            sviKorisnici.CanUserResizeColumns = false;
            sviKorisnici.CanUserResizeRows = false;
            sviKorisnici.AutoGenerateColumns = false;
            DataGridTextColumn ime = new DataGridTextColumn();
            ime.Header = "Ime";
            ime.Binding = new Binding("Ime");
            DataGridTextColumn prezime = new DataGridTextColumn();
            prezime.Header = "Prezime";
            prezime.Binding = new Binding("Prezime");
            DataGridTextColumn jmbg = new DataGridTextColumn();
            jmbg.Header = "JMBG";
            jmbg.Binding = new Binding("Jmbg");
            DataGridTextColumn username = new DataGridTextColumn();
            username.Header = "Korisnicko ime";
            username.Binding = new Binding("Username");
            DataGridTextColumn password = new DataGridTextColumn();
            password.Header = "Lozinka";
            password.Binding = new Binding("Password");
            DataGridTextColumn admin = new DataGridTextColumn();
            admin.Header = "Admin";
            admin.Binding = new Binding("Admin");
            DataGridTextColumn aktivnost = new DataGridTextColumn();
            aktivnost.Header = "Aktivan";
            aktivnost.Binding = new Binding("Aktivnost");
            sviKorisnici.Columns.Add(ime);
            sviKorisnici.Columns.Add(prezime);
            sviKorisnici.Columns.Add(jmbg);
            sviKorisnici.Columns.Add(username);
            sviKorisnici.Columns.Add(password);
            sviKorisnici.Columns.Add(admin);
            sviKorisnici.Columns.Add(aktivnost);
            cvs = new CollectionViewSource();
            Aplikacija.Instanca.UcitajKorisnike();
            cvs.Source = Aplikacija.Instanca.Korisnici;
            sviKorisnici.ItemsSource = cvs.View;
            sviKorisnici.IsSynchronizedWithCurrentItem = true;
            cvs.Filter += new FilterEventHandler(Pretraga);
            osveziBox();
        }
        private void osveziBox()
        {
            Searchbox.Text = "";
            cbAktivni.IsChecked = false;
        }
        private void add_Click(object sender, RoutedEventArgs e)
        {
            DodavanjeIizmenaKorisnika dk = new DodavanjeIizmenaKorisnika(false,new Korisnik());
            dk.ShowDialog();
            osveziBox();
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            Korisnik k = (Korisnik)sviKorisnici.SelectedItem;
            if (k!=null)
            {
                Korisnik original = (Korisnik)k.Clone();
                DodavanjeIizmenaKorisnika dk = new DodavanjeIizmenaKorisnika(true, k);
                if (dk.ShowDialog()!=true)
                {
                    int index = Aplikacija.Instanca.Korisnici.IndexOf(k);
                    Aplikacija.Instanca.Korisnici[index] = original;
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            osveziBox();
        }
        private void Searchbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Searchbox.Text == "")
            {
                lblPretraga.Visibility = Visibility.Visible;
            }else
            {
                lblPretraga.Visibility = Visibility.Hidden;
            }
            cvs.Filter += new FilterEventHandler(Pretraga);
        }

        private void cbAktivni_Checked(object sender, RoutedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(Pretraga);
        }
        private void Pretraga(object sender, FilterEventArgs e)
        {
            Korisnik k = e.Item as Korisnik;
            if (k != null)
            {
                bool cbAktivnost = true;
                if (cbAktivni.IsChecked == true)
                {
                    cbAktivnost = false;
                }
                if (k.Ime.ToLower().Contains(Searchbox.Text.ToLower()) && cbAktivnost && k.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else if (k.Ime.ToLower().Contains(Searchbox.Text.ToLower()) && !cbAktivnost)
                {
                    e.Accepted = true;
                }
                else if (k.Prezime.ToLower().Contains(Searchbox.Text.ToLower()) && cbAktivnost && k.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else if (k.Prezime.ToLower().Contains(Searchbox.Text.ToLower()) && !cbAktivnost)
                {
                    e.Accepted = true;
                }
                else if (k.Username.ToLower().Contains(Searchbox.Text.ToLower())&& cbAktivnost && k.Aktivnost == Aktivnost.Aktivan)
                {
                        e.Accepted = true;
                }
                else if (k.Username.ToLower().Contains(Searchbox.Text.ToLower()) && !cbAktivnost)
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                }
        }
    }

        private void lblPretraga_GotFocus(object sender, RoutedEventArgs e)
        {
            Searchbox.Focus();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
                Korisnik k = (Korisnik)sviKorisnici.SelectedItem;
            if (k != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda brisanja", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    k.Aktivnost = Aktivnost.NeAktivan;
                    int index = Aplikacija.Instanca.Korisnici.IndexOf(k);
                    Aplikacija.Instanca.Korisnici[index] = k;

                    KorisnikDAO.Update(k);
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            osveziBox();
        }
    }
}

