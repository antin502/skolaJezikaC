﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.GUI.FormeDodavanje;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Menadzeri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.Admin
{
    /// <summary>
    /// Interaction logic for NastavniciMenu.xaml
    /// </summary>
    public partial class NastavniciMenu : Window
    {
        CollectionViewSource cvs;
        public NastavniciMenu()
        {
            InitializeComponent();
            sviNastavnici.CanUserReorderColumns = false;
            sviNastavnici.IsReadOnly = true;
            sviNastavnici.SelectionUnit = DataGridSelectionUnit.FullRow;
            sviNastavnici.SelectionMode = DataGridSelectionMode.Single;
            sviNastavnici.CanUserResizeColumns = false;
            sviNastavnici.CanUserResizeRows = false;
            sviNastavnici.AutoGenerateColumns = false;
            DataGridTextColumn ime = new DataGridTextColumn();
            ime.Header = "Ime";
            ime.Binding = new Binding("Ime");
            DataGridTextColumn prezime = new DataGridTextColumn();
            prezime.Header = "Prezime";
            prezime.Binding = new Binding("Prezime");
            DataGridTextColumn jmbg = new DataGridTextColumn();
            jmbg.Header = "JMBG";
            jmbg.Binding = new Binding("Jmbg");
            DataGridTextColumn plata = new DataGridTextColumn();
            plata.Header = "Plata";
            plata.Binding = new Binding("Plata");
            sviNastavnici.Columns.Add(ime);
            sviNastavnici.Columns.Add(prezime);
            sviNastavnici.Columns.Add(jmbg);
            sviNastavnici.Columns.Add(plata);
            cvs= new CollectionViewSource();
            Aplikacija.Instanca.ucitajNastavnike("");
            cvs.Source = Aplikacija.Instanca.Nastavnici;
            sviNastavnici.ItemsSource = cvs.View;
            sviNastavnici.IsSynchronizedWithCurrentItem = true;
            cvs.Filter += new FilterEventHandler(Pretraga);
            osveziBox();
        }
        private void osveziBox()
        {
            SearchBox.Text = "";
        }
        private void addNastavnik_Click(object sender, RoutedEventArgs e)
        {
            DodavanjeIizmenaNastavnika dn = new DodavanjeIizmenaNastavnika(false, new Nastavnik());
            dn.ShowDialog();
            osveziBox();
        }

        private void editNastavnik_Click(object sender, RoutedEventArgs e)
        {
            Nastavnik nastavnik = (Nastavnik)sviNastavnici.SelectedItem;
            if(nastavnik!=null)
            {
                Nastavnik original = (Nastavnik)nastavnik.Clone();
                DodavanjeIizmenaNastavnika dn = new DodavanjeIizmenaNastavnika(true, nastavnik);
                if (dn.ShowDialog() != true)
                {
                    int index = Aplikacija.Instanca.Nastavnici.IndexOf(nastavnik);
                    Aplikacija.Instanca.Nastavnici[index] = original;
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            osveziBox();
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchBox.Text == "")
            {
                txtPretraga.Visibility = Visibility.Visible;
            }
            else
            {
                txtPretraga.Visibility = Visibility.Hidden;
            }
            cvs.Filter += new FilterEventHandler(Pretraga);
        }


        private void Pretraga(object sender, FilterEventArgs e)
        {
            Nastavnik n = e.Item as Nastavnik;
            if (n != null)
            {

                if (n.Ime.ToLower().Contains(SearchBox.Text.ToLower()) && n.Aktivnost==Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }

                else if (n.Prezime.ToLower().Contains(SearchBox.Text.ToLower()) && n.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }

                else if (n.Jmbg.ToLower().Contains(SearchBox.Text.ToLower()) && n.Aktivnost == Aktivnost.Aktivan)
                {
                        e.Accepted = true;
                }

                else
                {
                    e.Accepted = false;
                }
            }
        }

        private void txtPretraga_GotFocus(object sender, RoutedEventArgs e)
        {
            SearchBox.Focus();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Nastavnik nastavnik = (Nastavnik)sviNastavnici.SelectedItem;
            if (nastavnik != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda brisanja", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    nastavnik.Aktivnost = Aktivnost.NeAktivan;
                    int index = Aplikacija.Instanca.Nastavnici.IndexOf(nastavnik);
                    Aplikacija.Instanca.Nastavnici[index] = nastavnik;
                    NastavnikDAO.Update(nastavnik);
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            osveziBox();
        }
    }
}
