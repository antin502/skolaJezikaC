﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeIizmenaTipovaKursa.xaml
    /// </summary>
    public partial class DodavanjeIizmenaTipovaKursa : Window
    {
        private bool Izmena;
        private TipKursa tipKursa;

        public DodavanjeIizmenaTipovaKursa(bool izmena,TipKursa t)
        {
            InitializeComponent();
            this.Izmena = izmena;
            this.tipKursa = t;
            this.DataContext = tipKursa;
            if (izmena)
            {
                this.Title = "Izmena tipa kursa";
            }
            else
            {
                this.Title = "Dodavanje tipa kursa";
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (txtNaziv.Text == "") { }
            else if (Validation.GetHasError(txtNaziv)) { }

            else if (!Izmena)
            {
                this.DialogResult = true;
                Aplikacija.Instanca.Tipovi.Add(this.tipKursa);
                TipDAO.Create(tipKursa);
                MessageBox.Show("Usepšno dodat tip kursa", "Super");
            } else if (Izmena) {
                this.DialogResult = true;
                TipDAO.Update(tipKursa);
                MessageBox.Show("Uspešno si izmenio tip kursa","Super");
            }
        }
    }
}
