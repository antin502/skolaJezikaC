﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeIizmenaUcenika.xaml
    /// </summary>
    public partial class DodavanjeIizmenaUcenika : Window
    {
        private bool Izmena;
        private Ucenik Ucenik;
        private CollectionViewSource cvsUplate;
        private CollectionViewSource cvsKursevi;
        public DodavanjeIizmenaUcenika(bool izmena, Ucenik u, bool ucenikLog=false)
        {
            InitializeComponent();
            this.Ucenik = u;
            this.Izmena = izmena;
            this.DataContext = Ucenik;
            if (Izmena)
            {
                this.Title = "Izmena učenika";
                initTableKursevi();
                initTableUplate();
                txtJmbg.IsEnabled = false;
                btnInfo.Visibility = Visibility.Hidden;
                duzinaJMBG.Visibility = Visibility.Hidden;
            }
            else if (ucenikLog)
            {
                this.Title = "Informacije o uceniku";
                txtName.IsEnabled = false;
                txtSurname.IsEnabled = false;
                txtJmbg.IsEnabled = false;
                btnOk.Visibility = Visibility.Hidden;
                btnCancel.Visibility = Visibility.Hidden;
                btnInfo.Visibility = Visibility.Hidden;
                duzinaJMBG.Visibility = Visibility.Hidden;
                initTableKursevi();
                initTableUplate();
            }
            else
            {
                this.Title = "Dodavanje učenika";
                tiKursevi.Visibility = Visibility.Hidden;
                tiUplate.Visibility = Visibility.Hidden;

            }
        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (txtSurname.Text == "" || txtSurname.Text == "" || txtJmbg.Text == "") { }
            if (Validation.GetHasError(txtName) || Validation.GetHasError(txtSurname) || Validation.GetHasError(txtJmbg)) { }
            else if (!Izmena)
            {
                this.DialogResult = true;
                Aplikacija.Instanca.Ucenici.Add(Ucenik);
                UcenikDAO.Create(this.Ucenik);
                MessageBox.Show("Uspešno dodat učenik", "Super");
            }
            else if (Izmena)
            {
                this.DialogResult = true;
                UcenikDAO.Update(this.Ucenik);
                MessageBox.Show("Uspešno izmenjen učenik", "Super");
            }

        }
        private void btnInfo_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("JMBG mora biti broj, mora biti dužine 13 karaktera i mora biti jedinstven (da ne postoji u bazi)!", "INFO");
        }

        private void txtJmbg_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Utils.CheckIsNumeric(e);
        }

        private void initTableUplate()
        {
            dgUplate.CanUserReorderColumns = false;
            dgUplate.IsReadOnly = true;
            dgUplate.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgUplate.SelectionMode = DataGridSelectionMode.Single;
            dgUplate.CanUserResizeColumns = false;
            dgUplate.CanUserResizeRows = false;
            dgUplate.AutoGenerateColumns = false;
            DataGridTextColumn oznaka = new DataGridTextColumn();
            oznaka.Header = "Oznaka";
            oznaka.Binding = new Binding("Id");
            DataGridTextColumn kurs = new DataGridTextColumn();
            kurs.Header = "Kurs";
            kurs.Binding = new Binding("Kurs");
            DataGridTextColumn datum = new DataGridTextColumn();
            datum.Header = "Datum uplate";
            datum.Binding = new Binding("DatumUplate");
            DataGridTextColumn cena = new DataGridTextColumn();
            cena.Header = "Cena";
            cena.Binding = new Binding("Cena");
            dgUplate.Columns.Add(oznaka);
            dgUplate.Columns.Add(kurs);
            dgUplate.Columns.Add(datum);
            dgUplate.Columns.Add(cena);
            cvsUplate = new CollectionViewSource();
            Aplikacija.Instanca.ucitajUplate();
            cvsUplate.Source = Aplikacija.Instanca.Uplate;
            dgUplate.ItemsSource = cvsUplate.View;
            dgUplate.IsSynchronizedWithCurrentItem = true;
            cvsUplate.Filter += new FilterEventHandler(PripadaUceniku);
        }

        private void PripadaUceniku(object sender, FilterEventArgs e)
        {
            Uplata u = e.Item as Uplata;
            if (u != null)
            {
                e.Accepted = (u.Kurs.Aktivnost==Aktivnost.Aktivan && u.Aktivnost == Aktivnost.Aktivan && u.Uplatilac.Jmbg == Ucenik.Jmbg);
            }
        }
        private void initTableKursevi()
        {
            dgKursevi.CanUserReorderColumns = false;
            dgKursevi.IsReadOnly = true;
            dgKursevi.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgKursevi.SelectionMode = DataGridSelectionMode.Single;
            dgKursevi.CanUserResizeColumns = false;
            dgKursevi.CanUserResizeRows = false;
            dgKursevi.AutoGenerateColumns = false;
            DataGridTextColumn oznaka = new DataGridTextColumn();
            oznaka.Header = "Oznaka";
            oznaka.Binding = new Binding("kurs.OznakaKursa");
            DataGridTextColumn jezik = new DataGridTextColumn();
            jezik.Header = "Jezik";
            jezik.Binding = new Binding("kurs.StraniJezik.Naziv");
            DataGridTextColumn tipKursa = new DataGridTextColumn();
            tipKursa.Header = "Tip Kursa";
            tipKursa.Binding = new Binding("kurs.NivoKursa.Naziv");
            DataGridTextColumn dtPocetka = new DataGridTextColumn();
            dtPocetka.Header = "Datum pocetka";
            dtPocetka.Binding = new Binding("kurs.DatePocetak.Date");
            dtPocetka.Binding.StringFormat = "dd.mm.yyyy.";
            DataGridTextColumn dtKraj = new DataGridTextColumn();
            dtKraj.Header = "Datum kraja";
            dtKraj.Binding = new Binding("kurs.DateKraj.Date");
            dtKraj.Binding.StringFormat = "dd.mm.yyyy.";
            DataGridTextColumn cena = new DataGridTextColumn();
            cena.Header = "Cena";
            cena.Binding = new Binding("kurs.Cena");
            DataGridTextColumn nastavnik = new DataGridTextColumn();
            nastavnik.Header = "Nastavnik";
            nastavnik.Binding = new Binding("kurs.Predavac");
            dgKursevi.Columns.Add(oznaka);
            dgKursevi.Columns.Add(tipKursa);
            dgKursevi.Columns.Add(jezik);
            dgKursevi.Columns.Add(dtPocetka);
            dgKursevi.Columns.Add(dtKraj);
            dgKursevi.Columns.Add(cena);
            dgKursevi.Columns.Add(nastavnik);
            cvsKursevi = new CollectionViewSource();
            Aplikacija.Instanca.ucitajJezike("");
            Aplikacija.Instanca.ucitajTipove("");
            Aplikacija.Instanca.ucitajNastavnike("");
            Aplikacija.Instanca.ucitajKurseve("");
            Aplikacija.Instanca.ucitajUcenikeNaKursu("");
            cvsKursevi.Source = Aplikacija.Instanca.UceniciNaKursu;
            dgKursevi.ItemsSource = cvsKursevi.View;
            dgKursevi.IsSynchronizedWithCurrentItem = true;
            cvsKursevi.Filter += new FilterEventHandler(PripadaKursu);
        }
        private void PripadaKursu(object sender, FilterEventArgs e)
        {
            UcenikNaKurs u = e.Item as UcenikNaKurs;
            if (u != null)
            {
                    e.Accepted = (u.kurs.Aktivnost==Aktivnost.Aktivan && u.aktivnost == Aktivnost.Aktivan && u.ucenik.Jmbg == Ucenik.Jmbg);
            }
        }

        private void txtJmbg_TextChanged(object sender, TextChangedEventArgs e)
        {
            duzinaJMBG.Content = "Dužina: " + ((txtJmbg.Text).Length);
        }
    }
}
