﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeUplata.xaml
    /// </summary>
    public partial class DodavanjeUplata : Window
    {
        private CollectionViewSource cvsKursevi;
        private CollectionViewSource cvsUcenici;
        private Uplata uplata;
        public DodavanjeUplata()
        {
            InitializeComponent();
            uplata =  new Uplata();
            Skola sk = SkolaDAO.Read();
            txtZiroRacun.Text = sk.Zracun;
            this.DataContext = uplata;
            cvsKursevi = new CollectionViewSource();
            cvsKursevi.Source = Aplikacija.Instanca.Kursevi;
            cbKurs.ItemsSource = cvsKursevi.View;
            cbKurs.IsSynchronizedWithCurrentItem = true;

            cvsUcenici = new CollectionViewSource();
            cvsUcenici.Source = Aplikacija.Instanca.Ucenici;
            cbUcenik.ItemsSource = cvsUcenici.View;
            cbUcenik.IsSynchronizedWithCurrentItem = true;
            cvsUcenici.Filter += new FilterEventHandler(samoAktivniUcenici);
            cvsKursevi.Filter += new FilterEventHandler(samoAktivniKursevi);
            this.Title = "Dodavanje uplata";
        }
        private void samoAktivniKursevi(object sender, FilterEventArgs e)
        {
            Kurs.Kurs k = e.Item as Kurs.Kurs;
            if (k != null)
            {try
                {
                    e.Accepted = (k.Aktivnost == Aktivnost.Aktivan &&
                                    Aplikacija.Instanca.PronadjiUcenikaNaKursu(new UcenikNaKurs((Ucenik)cbUcenik.SelectedItem, k, Aktivnost.Aktivan)) != null);
                }
                catch (Exception ex)
                {
                    e.Accepted = false;
                }
            }
        }
        private void samoAktivniUcenici(object sender, FilterEventArgs e)
        {
            Ucenik u = e.Item as Ucenik;
            if (u != null)
            {
                e.Accepted = (u.Aktivnost == Aktivnost.Aktivan );
            }
        }


        private void btnIzvrsi_Click(object sender, RoutedEventArgs e)
        {
            if (Validation.GetHasError(txtCena) || txtCena.Text == "") { }
            else if (cbKurs.SelectedItem == null || cbUcenik.SelectedItem == null) { }
            else
            {
                this.DialogResult = true;
                Aplikacija.Instanca.Uplate.Add(uplata);
                UplataDAO.Create(uplata);
                MessageBox.Show("Uspešno dodata uplata", "Super");
            }
        }

        private void cbUcenik_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cvsKursevi.Filter += new FilterEventHandler(samoAktivniKursevi);
        }
    }
}
