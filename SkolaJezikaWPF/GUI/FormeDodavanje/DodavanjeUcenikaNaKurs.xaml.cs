﻿using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeUcenikaNaKurs.xaml
    /// </summary>
    public partial class DodavanjeUcenikaNaKurs : Window
    {
        private Kurs.Kurs kurs;
        public DodavanjeUcenikaNaKurs(Kurs.Kurs k)
        {
            InitializeComponent();
            this.kurs = k;
            txtKurs.Text= kurs.ToString();
            txtKurs.IsEnabled = false;
            CollectionViewSource cvs = new CollectionViewSource();
            Aplikacija.Instanca.ucitajUcenike();
            cvs.Source = Aplikacija.Instanca.Ucenici;
            cbUcenik.ItemsSource = cvs.View;
            cbUcenik.IsSynchronizedWithCurrentItem = true;
            cvs.Filter += new FilterEventHandler(aktivni);
            this.Title = "Dodavanje učenika na kurs";

        }
        private void aktivni(object sender, FilterEventArgs e)
        {
            Ucenik u = e.Item as Ucenik;
            if (u != null)
            {
                    e.Accepted = (Aplikacija.Instanca.PronadjiUcenikaNaKursu(new UcenikNaKurs(u, kurs, Aktivnost.Aktivan)) == null && u.Aktivnost == Aktivnost.Aktivan);
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            UcenikNaKurs u = new UcenikNaKurs((Ucenik)cbUcenik.SelectedItem, kurs, Aktivnost.Aktivan);
            Aplikacija.Instanca.UceniciNaKursu.Add(u);
            UceniciNaKursuDB.dodati.Add(u);
            this.DialogResult = true;
            MessageBox.Show("Uspesno dodat ucenik na kurs");
        }
    }
}
