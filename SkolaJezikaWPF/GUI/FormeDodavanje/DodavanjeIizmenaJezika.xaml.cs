﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeIizmenaJezika.xaml
    /// </summary>
    public partial class DodavanjeIizmenaJezika : Window
    {
        private bool Izmena;
        private Jezik jezik;
        private CollectionViewSource cvs;
        public DodavanjeIizmenaJezika(bool izmena, Jezik j)
        {
            InitializeComponent();
            this.Izmena = izmena;
            this.jezik = j;
            this.DataContext = jezik;
            if (izmena)
            {
                this.Title = "Izmena jezika";
            }
            else
            {
                this.Title = "Dodavanje jezika";
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == "") {}
            if (Validation.GetHasError(txtName)) { }
            else if (!Izmena)
            {
                this.DialogResult = true;
                Aplikacija.Instanca.Jezici.Add(jezik);
                JezikDao.Create(jezik);
                MessageBox.Show("Uspešno dodat jezik", "Super");
            }
            else if (Izmena)
            {
                this.DialogResult = true;
                JezikDao.Update(jezik);
                MessageBox.Show("Uspešno izmenjen jezik", "Super");
            }
        }
 

    }
}
