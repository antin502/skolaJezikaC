﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeIizmenaNastavnika.xaml
    /// </summary>
    public partial class DodavanjeIizmenaNastavnika : Window
    {
        private bool Izmena;
        private Nastavnik nastavnik;
        private CollectionViewSource cvsKursevi;

        public DodavanjeIizmenaNastavnika(bool izmena, Nastavnik nastavnik,bool nastavnikLog=false)
        {
            InitializeComponent();
            this.Izmena = izmena;
            this.nastavnik = nastavnik;
            this.DataContext = nastavnik;
            if (Izmena)
            {
                this.Title = "Izmena nastavnika";
                txtJMBG.IsEnabled = false;
                btnINFO.Visibility = Visibility.Hidden;
                duzinaJMBG.Visibility = Visibility.Hidden;
                initTableKursevi();
            }
            else if (nastavnikLog)
            {
                this.Title = "Informacije o ulogovanom nastavniku";
                txtIme.IsEnabled = false;
                txtPrezime.IsEnabled = false;
                txtJMBG.IsEnabled = false;
                txtPlata.IsEnabled = false;
                initTableKursevi();
                btnOk.Visibility = Visibility.Hidden;
                btnCancel.Visibility = Visibility.Hidden;
                btnINFO.Visibility = Visibility.Hidden;
                duzinaJMBG.Visibility = Visibility.Hidden;
            }
            else
            {
                this.Title = "Dodavanje nastavnika";
                dgKursevi.Visibility = Visibility.Hidden;
                tiKursevi.Visibility = Visibility.Hidden;
            }
        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (txtIme.Text == "" || txtPrezime.Text == "" || txtJMBG.Text=="" || txtPlata.Text == "") { }
            if (Validation.GetHasError(txtIme) || Validation.GetHasError(txtPrezime) || Validation.GetHasError(txtJMBG)) { }
            else if (!Izmena)
            {
                this.nastavnik.Aktivnost = Aktivnost.Aktivan;
                this.DialogResult = true;
                Aplikacija.Instanca.Nastavnici.Add(nastavnik);
                NastavnikDAO.Create(nastavnik);
                MessageBox.Show("Uspešno dodat nastavnik", "Super");
            }
            else if (Izmena)
            {
                this.DialogResult = true;
                NastavnikDAO.Update(nastavnik);
                MessageBox.Show("Uspešno izmenjen nastavnik", "Super");
            }

        }
        private void txtJMBG_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Utils.CheckIsNumeric(e);
        }
        private void txtPlata_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Utils.CheckIsNumeric(e);
        }
        private void btnINFO_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("JMBG mora biti broj, mora biti dužine 13 karaktera i mora biti jedinstven (da ne postoji u bazi)!", "INFO");
        }

        private void txtJMBG_TextChanged(object sender, TextChangedEventArgs e)
        {
            duzinaJMBG.Content = "Dužina: " + ((txtJMBG.Text).Length);
        }
        private void initTableKursevi()
        {
            dgKursevi.CanUserReorderColumns = false;
            dgKursevi.IsReadOnly = true;
            dgKursevi.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgKursevi.SelectionMode = DataGridSelectionMode.Single;
            dgKursevi.CanUserResizeColumns = false;
            dgKursevi.CanUserResizeRows = false;
            dgKursevi.AutoGenerateColumns = false;
            DataGridTextColumn oznaka = new DataGridTextColumn();
            oznaka.Header = "Oznaka";
            oznaka.Binding = new Binding("OznakaKursa");
            DataGridTextColumn jezik = new DataGridTextColumn();
            jezik.Header = "Jezik";
            jezik.Binding = new Binding("StraniJezik.Naziv");
            DataGridTextColumn tipKursa = new DataGridTextColumn();
            tipKursa.Header = "Tip Kursa";
            tipKursa.Binding = new Binding("NivoKursa.Naziv");
            DataGridTextColumn dtPocetka = new DataGridTextColumn();
            dtPocetka.Header = "Datum pocetka";
            dtPocetka.Binding = new Binding("DatePocetak.Date");
            dtPocetka.Binding.StringFormat = "dd.mm.yyyy.";
            DataGridTextColumn dtKraj = new DataGridTextColumn();
            dtKraj.Header = "Datum kraja";
            dtKraj.Binding = new Binding("DateKraj.Date");
            dtKraj.Binding.StringFormat = "dd.mm.yyyy.";
            DataGridTextColumn cena = new DataGridTextColumn();
            cena.Header = "Cena";
            cena.Binding = new Binding("Cena");
            dgKursevi.Columns.Add(oznaka);
            dgKursevi.Columns.Add(tipKursa);
            dgKursevi.Columns.Add(jezik);
            dgKursevi.Columns.Add(dtPocetka);
            dgKursevi.Columns.Add(dtKraj);
            dgKursevi.Columns.Add(cena);
            cvsKursevi = new CollectionViewSource();
            Aplikacija.Instanca.ucitajJezike("");
            Aplikacija.Instanca.ucitajTipove("");
            Aplikacija.Instanca.ucitajKurseve("");
            cvsKursevi.Source = Aplikacija.Instanca.Kursevi;
            dgKursevi.ItemsSource = cvsKursevi.View;
            dgKursevi.IsSynchronizedWithCurrentItem = true;
            cvsKursevi.Filter += new FilterEventHandler(PripadaKursu);
        }
        private void PripadaKursu(object sender, FilterEventArgs e)
        {
            Kurs.Kurs k = e.Item as Kurs.Kurs;
            if (k != null)
            {
                e.Accepted = (k.Aktivnost == Aktivnost.Aktivan && k.Predavac.Jmbg == nastavnik.Jmbg);
            }
        }
    }
}
