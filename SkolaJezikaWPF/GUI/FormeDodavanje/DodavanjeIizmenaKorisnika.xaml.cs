﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Menadzeri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeIizmenaKorisnika.xaml
    /// </summary>
    public partial class DodavanjeIizmenaKorisnika : Window
    {
        private bool Izmena;
        private Korisnik korisnik;
        public DodavanjeIizmenaKorisnika(bool izmena,Korisnik k)
        {
            InitializeComponent();
            this.Izmena = izmena;
            this.korisnik = k;
            this.DataContext = korisnik;
            var admin = Enum.GetValues(typeof(Korisnici.Admin)).Cast<Korisnici.Admin>();
            cbbAdmin.ItemsSource = admin.ToList();
            if (Izmena)
            {
                this.Title = "Izmena korisnika";
                txtJmbg.IsEnabled = false;
                txtUsername.IsEnabled = false;
                btnInfo.Visibility = Visibility.Hidden;
                duzinaJMBG.Visibility = Visibility.Hidden;
            }
            else
            {
                this.Title = "Dodavanje korisnika";
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "" || txtName.Text=="" || txtSurname.Text=="" || txtJmbg.Text == "") { }
            if (Validation.GetHasError(txtUsername) || Validation.GetHasError(txtPassword) ||
                Validation.GetHasError(txtName) || Validation.GetHasError(txtSurname) || Validation.GetHasError(txtJmbg)) { }
            else if (!Izmena)
            {
                //korisnik = new Korisnik(username, password, name, surname, jmbg, Aktivnost.Aktivan, admin);
                this.DialogResult = true;
                Aplikacija.Instanca.Korisnici.Add(korisnik);
                KorisnikDAO.Create(korisnik);
                MessageBox.Show("Uspešno dodat korisnik", "Super");
            }
            else if (Izmena)
            {
                KorisnikDAO.Update(this.korisnik);
                this.DialogResult = true;
                MessageBox.Show("Uspešno izmenjen korisnik","Super");
            }

        }
        private void txtJmbg_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Utils.CheckIsNumeric(e);
        }

        private void btnInfo_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("JMBG mora biti broj, mora biti dužine 13 karaktera i mora biti jedinstven (da ne postoji u bazi)!","INFO");
        }

        private void txtJmbg_TextChanged(object sender, TextChangedEventArgs e)
        {
            duzinaJMBG.Content = "Dužina: " + ((txtJmbg.Text).Length);

        }
    }
}
