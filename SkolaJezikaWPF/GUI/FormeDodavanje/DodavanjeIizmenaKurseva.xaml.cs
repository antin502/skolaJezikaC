﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.FormeDodavanje
{
    /// <summary>
    /// Interaction logic for DodavanjeIizmenaKurseva.xaml
    /// </summary>
    public partial class DodavanjeIizmenaKurseva : Window
    {
        private bool Izmena;
        private Kurs.Kurs kurs;
        private CollectionViewSource cvsJezici;
        private CollectionViewSource cvsUcenici;
        private CollectionViewSource cvsNastavnici;
        private CollectionViewSource cvsTipovi;
        private CollectionViewSource cvsUplate;
        public DodavanjeIizmenaKurseva(bool izmena,Kurs.Kurs k)
        {
            InitializeComponent();
            this.kurs = k;
            this.Izmena = izmena;
            this.DataContext = kurs;
            cvsJezici = new CollectionViewSource();
            cvsJezici.Source = Aplikacija.Instanca.Jezici;
            cbJezici.ItemsSource = cvsJezici.View;
            cbJezici.IsSynchronizedWithCurrentItem = true;
            cvsJezici.Filter += new FilterEventHandler(samoAktivniJezici);
            cvsTipovi = new CollectionViewSource();
            cvsTipovi.Source = Aplikacija.Instanca.Tipovi;
            cbTipKursa.ItemsSource = cvsTipovi.View;
            cbTipKursa.IsSynchronizedWithCurrentItem = true;
            cvsTipovi.Filter += new FilterEventHandler(samoAktivniTipovi);

            cvsNastavnici = new CollectionViewSource();
            cvsNastavnici.Source = Aplikacija.Instanca.Nastavnici;
            cbNastavnici.ItemsSource = cvsNastavnici.View;
            cbNastavnici.IsSynchronizedWithCurrentItem = true;
            cvsNastavnici.Filter += new FilterEventHandler(samoAktivniNastavnici);
            if (!Izmena)
            {
                this.Title = "Dodavanje Kursa";
                dgUplate.Visibility = Visibility.Hidden;
                dgUcenici.Visibility = Visibility.Hidden;
                btnDodaj.Visibility = Visibility.Hidden;
                btnObrisi.Visibility = Visibility.Hidden;
                tiUcenici.Visibility = Visibility.Hidden;
                tiUplate.Visibility = Visibility.Hidden;
            }
            else
            {
                this.Title = "Izmena Kursa";
                dpDatumKraja.IsEnabled = false;
                dpDatumPocetka.IsEnabled = false;
                initTableUcenici();
                initTableUplate();
                UceniciNaKursuDB.initValues();
            }
        }
        private void samoAktivniJezici(object sender, FilterEventArgs e)
        {
            Jezik j = e.Item as Jezik;
            if (j != null)
            {
                e.Accepted = (j.Aktivnost == Aktivnost.Aktivan);
            }
        }
        private void samoAktivniTipovi(object sender, FilterEventArgs e)
        {
            TipKursa t = e.Item as TipKursa;
            if (t != null)
            {
                e.Accepted = (t.Aktivnost == Aktivnost.Aktivan);
            }
        }
        private void samoAktivniNastavnici(object sender, FilterEventArgs e)
        {
            Nastavnik n = e.Item as Nastavnik;
            if (n != null)
            {
                e.Accepted = (n.Aktivnost == Aktivnost.Aktivan);
            }
        }
        private void initTableUcenici()
        {
            dgUcenici.CanUserReorderColumns = false;
            dgUcenici.IsReadOnly = true;
            dgUcenici.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgUcenici.SelectionMode = DataGridSelectionMode.Single;
            dgUcenici.CanUserResizeColumns = false;
            dgUcenici.CanUserResizeRows = false;
            dgUcenici.AutoGenerateColumns = false;
            DataGridTextColumn ime = new DataGridTextColumn();
            ime.Header = "Ime";
            ime.Binding = new Binding("ucenik.Ime");
            DataGridTextColumn prezime = new DataGridTextColumn();
            prezime.Header = "Prezime";
            prezime.Binding = new Binding("ucenik.Prezime");
            DataGridTextColumn jmbg = new DataGridTextColumn();
            jmbg.Header = "JMBG";
            jmbg.Binding = new Binding("ucenik.Jmbg");
            dgUcenici.Columns.Add(ime);
            dgUcenici.Columns.Add(prezime);
            dgUcenici.Columns.Add(jmbg);
            cvsUcenici = new CollectionViewSource();
            Aplikacija.Instanca.ucitajUcenikeNaKursu();
            cvsUcenici.Source = Aplikacija.Instanca.UceniciNaKursu;
            dgUcenici.ItemsSource = cvsUcenici.View;
            dgUcenici.IsSynchronizedWithCurrentItem = true;
            cvsUcenici.Filter += new FilterEventHandler(aktivni);

        }
        private void initTableUplate()
        {
            dgUplate.CanUserReorderColumns = false;
            dgUplate.IsReadOnly = true;
            dgUplate.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgUplate.SelectionMode = DataGridSelectionMode.Single;
            dgUplate.CanUserResizeColumns = false;
            dgUplate.CanUserResizeRows = false;
            dgUplate.AutoGenerateColumns = false;
            DataGridTextColumn oznaka = new DataGridTextColumn();
            oznaka.Header = "Oznaka";
            oznaka.Binding = new Binding("Id");
            DataGridTextColumn ucenik = new DataGridTextColumn();
            ucenik.Header = "Ucenik";
            ucenik.Binding = new Binding("Uplatilac");
            DataGridTextColumn datum = new DataGridTextColumn();
            datum.Header = "Datum uplate";
            datum.Binding = new Binding("DatumUplate");
            DataGridTextColumn cena = new DataGridTextColumn();
            cena.Header = "Cena";
            cena.Binding = new Binding("Cena");
            dgUplate.Columns.Add(oznaka);
            dgUplate.Columns.Add(ucenik);
            dgUplate.Columns.Add(datum);
            dgUplate.Columns.Add(cena);
            cvsUplate = new CollectionViewSource();
            Aplikacija.Instanca.ucitajUplate();
            cvsUplate.Source = Aplikacija.Instanca.Uplate;
            dgUplate.ItemsSource = cvsUplate.View;
            dgUplate.IsSynchronizedWithCurrentItem = true;
            cvsUplate.Filter += new FilterEventHandler(naKursu);
        }
        private void aktivni(object sender, FilterEventArgs e)
        {
            UcenikNaKurs u = e.Item as UcenikNaKurs;
            if (u != null)
            {
                e.Accepted = (u.aktivnost == Aktivnost.Aktivan && u.kurs.OznakaKursa == kurs.OznakaKursa);
            }
        }
        private void naKursu(object sender, FilterEventArgs e)
        {
            Uplata u = e.Item as Uplata;
            if (u != null)
            {
                    e.Accepted = u.Aktivnost == Aktivnost.Aktivan && u.Kurs.OznakaKursa == kurs.OznakaKursa;
            }
        }

        private void txtCena_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Utils.CheckIsNumeric(e);
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            DodavanjeUcenikaNaKurs duk = new DodavanjeUcenikaNaKurs(this.kurs);
            duk.ShowDialog();
            cvsUcenici.Filter += new FilterEventHandler(aktivni);
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            bool ok = true;
            if (dpDatumPocetka.SelectedDate > dpDatumKraja.SelectedDate && !Izmena)
            {
                dpDatumKraja.BorderBrush = Brushes.Red;
                ok = false;
            }
            else
            {
                dpDatumKraja.ClearValue(TextBox.BorderBrushProperty);
            }
            if(cbJezici.SelectedItem == null || cbNastavnici.SelectedItem==null || cbTipKursa.SelectedItem == null)
            {
                ok = false;
            }
            if (dpDatumPocetka.SelectedDate <DateTime.Today && !Izmena)
            {
                dpDatumPocetka.BorderBrush = Brushes.Red;
                ok = false;
            }
            else
            {
                dpDatumPocetka.ClearValue(TextBox.BorderBrushProperty);
            }
            if (!ok || txtCena.Text=="" || Validation.GetHasError(txtCena)) { }
            else if (!Izmena)
            {
                this.DialogResult = true;
                Aplikacija.Instanca.Kursevi.Add(kurs);
                KursDAO.Create(kurs);
                MessageBox.Show("Uspešno dodat Kurs", "Super");
            }
            else if (Izmena)
            {
                KursDAO.Update(kurs);
                foreach (UcenikNaKurs u in UceniciNaKursuDB.dodati)
                {
                    UcenikNaKursuDAO.Create(u);
                }
                foreach (UcenikNaKurs u in UceniciNaKursuDB.obrisani)
                {
                    UcenikNaKursuDAO.Delete(u);
                }
                this.DialogResult = true;
                MessageBox.Show("Uspešno izmenjen Kurs", "Super");
            }

        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            UcenikNaKurs ucenik = (UcenikNaKurs)dgUcenici.SelectedItem;
            if (ucenik != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda brisanja", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    ucenik.aktivnost = Aktivnost.NeAktivan;
                    int index = Aplikacija.Instanca.UceniciNaKursu.IndexOf(ucenik);
                    Aplikacija.Instanca.UceniciNaKursu[index] = ucenik;
                    UceniciNaKursuDB.obrisani.Add(ucenik);
                    foreach (UcenikNaKurs u in UceniciNaKursuDB.dodati)
                    {
                        if (u == ucenik)
                        {
                            UceniciNaKursuDB.dodati.Remove(u);
                            break;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }

            cvsUcenici.Filter += new FilterEventHandler(aktivni);
        }

    }
}
