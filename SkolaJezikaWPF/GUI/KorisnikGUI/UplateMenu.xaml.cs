﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.KorisnikGUI
{
    /// <summary>
    /// Interaction logic for UplateMenu.xaml
    /// </summary>
    public partial class UplateMenu : Window
    {
        private CollectionViewSource cvs;
        public UplateMenu()
        {
            InitializeComponent();
            dgUplate.CanUserReorderColumns = false;
            dgUplate.IsReadOnly = true;
            dgUplate.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgUplate.SelectionMode = DataGridSelectionMode.Single;
            dgUplate.CanUserResizeColumns = false;
            dgUplate.CanUserResizeRows = false;
            dgUplate.AutoGenerateColumns = false;
            DataGridTextColumn oznaka = new DataGridTextColumn();
            oznaka.Header = "Oznaka";
            oznaka.Binding = new Binding("Id");
            DataGridTextColumn ucenik = new DataGridTextColumn();
            ucenik.Header = "Ucenik";
            ucenik.Binding = new Binding("Uplatilac");
            DataGridTextColumn kurs = new DataGridTextColumn();
            kurs.Header = "Kurs";
            kurs.Binding = new Binding("Kurs");
            DataGridTextColumn datum = new DataGridTextColumn();
            datum.Header = "Datum uplate";
            datum.Binding = new Binding("DatumUplate");
            DataGridTextColumn cena = new DataGridTextColumn();
            cena.Header = "Cena";
            cena.Binding = new Binding("Cena");
            dgUplate.Columns.Add(oznaka);
            dgUplate.Columns.Add(kurs);
            dgUplate.Columns.Add(ucenik);
            dgUplate.Columns.Add(datum);
            dgUplate.Columns.Add(cena);
            cvs = new CollectionViewSource();
            Aplikacija.Instanca.ucitajJezike("");
            Aplikacija.Instanca.ucitajNastavnike("");
            Aplikacija.Instanca.ucitajUcenike("");
            Aplikacija.Instanca.ucitajTipove("");
            Aplikacija.Instanca.ucitajKurseve("");
            Aplikacija.Instanca.ucitajUplate("");
            Aplikacija.Instanca.ucitajUcenikeNaKursu();
            cvs.Source = Aplikacija.Instanca.Uplate;
            dgUplate.ItemsSource = cvs.View;
            dgUplate.IsSynchronizedWithCurrentItem = true;
            cvs.Filter += new FilterEventHandler(aktivni);
            
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            FormeDodavanje.DodavanjeUplata du = new FormeDodavanje.DodavanjeUplata();
            du.ShowDialog();
        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Uplata uplata = (Uplata)dgUplate.SelectedItem;
            if (uplata != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda brisanja", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    uplata.Aktivnost = Aktivnost.NeAktivan;
                    int index = Aplikacija.Instanca.Uplate.IndexOf(uplata);
                    Aplikacija.Instanca.Uplate[index] = uplata;
                    UplataDAO.Delete(uplata);
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }

            cvs.Filter += new FilterEventHandler(aktivni);
        }
        private void aktivni(object sender, FilterEventArgs e)
        {
            Uplata u = e.Item as Uplata;
            if (u != null)
            {
                if (u.Aktivnost == Aktivnost.Aktivan && u.Kurs.StraniJezik.Naziv.ToLower().Contains(txtSearch.Text.ToLower()))
                {
                    e.Accepted = true;
                }
                else if (u.Aktivnost == Aktivnost.Aktivan && u.Kurs.NivoKursa.Naziv.ToLower().Contains(txtSearch.Text.ToLower()))
                {
                    e.Accepted = true;
                }
                else if (u.Aktivnost == Aktivnost.Aktivan && u.Uplatilac.Ime.ToLower().Contains(txtSearch.Text.ToLower()))
                {
                    e.Accepted = true;
                }
                else if (u.Aktivnost == Aktivnost.Aktivan && u.Uplatilac.Prezime.ToLower().Contains(txtSearch.Text.ToLower()))
                {
                    e.Accepted = true;
                }
                else if (u.Aktivnost == Aktivnost.Aktivan && u.Uplatilac.Jmbg.ToLower().Contains(txtSearch.Text.ToLower()))
                {
                    e.Accepted = true;
                }

                else
                {
                    e.Accepted = false;
                }
            }
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text == "")
            {
                lblPretraga.Visibility = Visibility.Visible;
            }
            else
            {
                lblPretraga.Visibility = Visibility.Hidden;
            }

            cvs.Filter += new FilterEventHandler(aktivni);
        }

        private void lblPretraga_GotFocus(object sender, RoutedEventArgs e)
        {
            txtSearch.Focus();
        }
    }
}
