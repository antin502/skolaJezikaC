﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.GUI.FormeDodavanje;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Menadzeri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.KorisnikGUI
{
    /// <summary>
    /// Interaction logic for UceniciMenu.xaml
    /// </summary>
    public partial class UceniciMenu : Window
    {
        CollectionViewSource cvs;
        public UceniciMenu()
        {
            InitializeComponent();
            sviUcenici.CanUserReorderColumns = false;
            sviUcenici.IsReadOnly = true;
            sviUcenici.SelectionUnit = DataGridSelectionUnit.FullRow;
            sviUcenici.SelectionMode = DataGridSelectionMode.Single;
            sviUcenici.CanUserResizeColumns = false;
            sviUcenici.CanUserResizeRows = false;
            sviUcenici.AutoGenerateColumns = false;
            DataGridTextColumn ime = new DataGridTextColumn();
            ime.Header = "Ime";
            ime.Binding = new Binding("Ime");
            DataGridTextColumn prezime = new DataGridTextColumn();
            prezime.Header = "Prezime";
            prezime.Binding = new Binding("Prezime");
            DataGridTextColumn jmbg = new DataGridTextColumn();
            jmbg.Header = "JMBG";
            jmbg.Binding = new Binding("Jmbg");
            sviUcenici.Columns.Add(ime);
            sviUcenici.Columns.Add(prezime);
            sviUcenici.Columns.Add(jmbg);
            cvs = new CollectionViewSource();
            Aplikacija.Instanca.ucitajUcenike("");
            cvs.Source = Aplikacija.Instanca.Ucenici;
            sviUcenici.ItemsSource = cvs.View;
            sviUcenici.IsSynchronizedWithCurrentItem = true;
            cvs.Filter += new FilterEventHandler(Pretraga);
            osveziBox();
        }
        private void osveziBox()
        {
            txtSearchBox.Text = "";
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            DodavanjeIizmenaUcenika du = new DodavanjeIizmenaUcenika(false, new Ucenik());
            du.ShowDialog();
            osveziBox();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Ucenik ucenik = (Ucenik)sviUcenici.SelectedItem;
            if(ucenik!=null)
            {
                Ucenik original = (Ucenik)ucenik.Clone();
                DodavanjeIizmenaUcenika iu = new DodavanjeIizmenaUcenika(true, ucenik);
                if (iu.ShowDialog() != true)
                {
                    int index = Aplikacija.Instanca.Ucenici.IndexOf(ucenik);
                    Aplikacija.Instanca.Ucenici[index] = original;
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }

            osveziBox();
        }
        private void txtSearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearchBox.Text == "")
            {
                lblPretraga.Visibility = Visibility.Visible;
            }
            else
            {
                lblPretraga.Visibility = Visibility.Hidden;
            }
            cvs.Filter += new FilterEventHandler(Pretraga);
        }

        private void cbAktivni_Checked(object sender, RoutedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(Pretraga);
        }
        private void Pretraga(object sender, FilterEventArgs e)
        {
            Ucenik u = e.Item as Ucenik;
            if (u != null)
            {
                if (u.Ime.ToLower().Contains(txtSearchBox.Text.ToLower()) && u.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else if (u.Prezime.ToLower().Contains(txtSearchBox.Text.ToLower()) && u.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else if (u.Jmbg.ToLower().Contains(txtSearchBox.Text.ToLower()) && u.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                }
            }
        }

        private void lblPretraga_GotFocus(object sender, RoutedEventArgs e)
        {
            txtSearchBox.Focus();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Ucenik u = (Ucenik)sviUcenici.SelectedItem;
            if (u != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda brisanja", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    u.Aktivnost = Aktivnost.NeAktivan;
                    int index = Aplikacija.Instanca.Ucenici.IndexOf(u);
                    Aplikacija.Instanca.Ucenici[index] = u;
                    UcenikDAO.Update(u);
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            osveziBox();
        }
        }
}
