﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI.KorisnikGUI
{
    /// <summary>
    /// Interaction logic for KursMenu.xaml
    /// </summary>
    public partial class KursMenu : Window
    {
        private CollectionViewSource cvs;
        public KursMenu()
        {
            InitializeComponent();
            dgKursevi.CanUserReorderColumns = false;
            dgKursevi.IsReadOnly = true;
            dgKursevi.SelectionUnit = DataGridSelectionUnit.FullRow;
            dgKursevi.SelectionMode = DataGridSelectionMode.Single;
            dgKursevi.CanUserResizeColumns = false;
            dgKursevi.CanUserResizeRows = false;
            dgKursevi.AutoGenerateColumns = false;
            DataGridTextColumn oznaka = new DataGridTextColumn();
            oznaka.Header = "Oznaka";
            oznaka.Binding = new Binding("OznakaKursa");
            DataGridTextColumn jezik = new DataGridTextColumn();
            jezik.Header = "Jezik";
            jezik.Binding = new Binding("StraniJezik.Naziv");
            DataGridTextColumn tipKursa = new DataGridTextColumn();
            tipKursa.Header = "Tip Kursa";
            tipKursa.Binding = new Binding("NivoKursa.Naziv");
            DataGridTextColumn dtPocetka = new DataGridTextColumn();
            dtPocetka.Header = "Datum pocetka";
            dtPocetka.Binding = new Binding("DatePocetak.Date");
            dtPocetka.Binding.StringFormat = "dd.MM.yyyy.";
            DataGridTextColumn dtKraj = new DataGridTextColumn();
            dtKraj.Header = "Datum kraja";
            dtKraj.Binding = new Binding("DateKraj.Date");
            dtKraj.Binding.StringFormat = "dd.MM.yyyy.";
            DataGridTextColumn cena = new DataGridTextColumn();
            cena.Header = "Cena";
            cena.Binding = new Binding("Cena");
            DataGridTextColumn nastavnik = new DataGridTextColumn();
            nastavnik.Header = "Nastavnik";
            nastavnik.Binding = new Binding("Predavac");
            dgKursevi.Columns.Add(oznaka);
            dgKursevi.Columns.Add(tipKursa);
            dgKursevi.Columns.Add(jezik);
            dgKursevi.Columns.Add(dtPocetka);
            dgKursevi.Columns.Add(dtKraj);
            dgKursevi.Columns.Add(cena);
            dgKursevi.Columns.Add(nastavnik);
            cvs= new CollectionViewSource();
            Aplikacija.Instanca.ucitajNastavnike("");
            Aplikacija.Instanca.ucitajUcenike("");
            Aplikacija.Instanca.ucitajTipove("");
            Aplikacija.Instanca.ucitajJezike("");
            Aplikacija.Instanca.ucitajKurseve("");
            cvs.Source = Aplikacija.Instanca.Kursevi;
            dgKursevi.ItemsSource = cvs.View;
            dgKursevi.IsSynchronizedWithCurrentItem = true;
            cvs.Filter += new FilterEventHandler(Pretraga);
        }
        private void OsveziBox()
        {
            txtSearch.Text = "";
            cvs.Filter += new FilterEventHandler(Pretraga);
        }
        private void Pretraga(object sender, FilterEventArgs e)
        {
            Kurs.Kurs k = e.Item as Kurs.Kurs;
            if (k != null)
            {
                if (k.NivoKursa.Naziv.ToLower().Contains(txtSearch.Text.ToLower()) && k.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else if (k.StraniJezik.Naziv.ToLower().Contains(txtSearch.Text.ToLower()) && k.Aktivnost == Aktivnost.Aktivan)
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                }
            }
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text == "")
            {
                lblPretraga.Visibility = Visibility.Visible;
            }
            else
            {
                lblPretraga.Visibility = Visibility.Hidden;
            }

            cvs.Filter += new FilterEventHandler(Pretraga);
        }

        private void cbAktivni_Checked(object sender, RoutedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(Pretraga);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            FormeDodavanje.DodavanjeIizmenaKurseva dk = new FormeDodavanje.DodavanjeIizmenaKurseva(false,new Kurs.Kurs());
            dk.ShowDialog();
            OsveziBox();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Kurs.Kurs kurs = (Kurs.Kurs)dgKursevi.SelectedItem;
            ObservableCollection<UcenikNaKurs> uceniciNaKursu =Aplikacija.Instanca.DeepCopyUcenikNaKursu();
            if (kurs != null)
            {
                Kurs.Kurs original = (Kurs.Kurs)kurs.Clone();
                FormeDodavanje.DodavanjeIizmenaKurseva dn = new FormeDodavanje.DodavanjeIizmenaKurseva(true, kurs);
                if (dn.ShowDialog() != true)
                {
                    int index = Aplikacija.Instanca.Kursevi.IndexOf(kurs);
                    Aplikacija.Instanca.Kursevi[index] = original;
                    Aplikacija.Instanca.UceniciNaKursu = new ObservableCollection<UcenikNaKurs>(uceniciNaKursu);
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            OsveziBox();
        }

        private void lblPretraga_GotFocus(object sender, RoutedEventArgs e)
        {
            txtSearch.Focus();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Kurs.Kurs k = (Kurs.Kurs)dgKursevi.SelectedItem;
            if (k != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda brisanja", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    k.Aktivnost = Aktivnost.NeAktivan;
                    int index = Aplikacija.Instanca.Kursevi.IndexOf(k);
                    Aplikacija.Instanca.Kursevi[index] = k;
                    KursDAO.Update(k);
                }
            }
            else
            {
                MessageBox.Show("Red nije selektovan", "Greška");
            }
            OsveziBox();
        
    }
    }
}
