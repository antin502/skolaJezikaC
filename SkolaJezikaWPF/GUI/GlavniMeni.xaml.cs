﻿using SkolaJezikaWPF.GUI.KorisnikGUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SkolaJezikaWPF.GUI
{
    /// <summary>
    /// Interaction logic for GlavniMeni.xaml
    /// </summary>
    public partial class GlavniMeni : Window
    {
        public GlavniMeni()
        {
            InitializeComponent();
        }

        private void btnJezici_Click(object sender, RoutedEventArgs e)
        {
            JeziciMenu jm = new JeziciMenu();
            jm.ShowDialog();
        }

        private void btnUcenici_Click(object sender, RoutedEventArgs e)
        {
            UceniciMenu um = new UceniciMenu();
            um.ShowDialog();
        }

        private void btnTipovi_Click(object sender, RoutedEventArgs e)
        {
            TipKursaMenu tkm = new TipKursaMenu();
            tkm.ShowDialog();
        }

        private void btnKursevi_Click(object sender, RoutedEventArgs e)
        {
            KursMenu km = new KursMenu();
            km.ShowDialog();
        }

        private void btnUplata_Click(object sender, RoutedEventArgs e)
        {
            UplateMenu um = new UplateMenu();
            um.ShowDialog();
        }
    }
}
