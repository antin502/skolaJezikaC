﻿using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Korisnici
{
    public class Nastavnik : Osoba, ICloneable
    {
        public double Plata { get; set; }
        public Nastavnik():base()
        {
            this.Plata = 0;
        }
        public Nastavnik(string ime, string prezime, string jmbg, double plata, Aktivnost aktivan) : base(ime, prezime, jmbg,aktivan)
        {
            this.Plata = plata;
        }
        public override string ToString()
        {
            return this.Ime + " " + this.Prezime;
        }

        public object Clone()
        {
            Nastavnik original = new Nastavnik();
            original.Ime = Ime;
            original.Prezime = Prezime;
            original.Jmbg = Jmbg;
            original.Plata = Plata;
            original.Aktivnost = Aktivnost;
            return original;
        }
    }
}
