﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Korisnici
{

    public class Korisnik : Osoba, ICloneable
    {

        public string Username { get; set; }
        public string Password { get; set; }
        public Admin Admin { get; set; }
        public Korisnik():base()
        {
            this.Username = "";
            this.Password = "";
            this.Admin = Admin.Nije;
        }

        public Korisnik(string username, string password, string ime,
            string prezime, string jmbg, Aktivnost a, Admin admin) : base(ime, prezime, jmbg,a)
        {
            this.Username = username;
            this.Password = password;
            this.Admin = admin;
        }
        public override string ToString()
        {
            return base.ToString() + "\nKorisnicko ime: " + this.Username + "\nLozinka:" + this.Password +
                "\nAktivnost: " + this.Aktivnost + "\nAdmin: " + this.Admin;
        }

        public object Clone()
        {
            Korisnik original = new Korisnik();
            original.Username = Username;
            original.Password = Password;
            original.Ime = Ime;
            original.Prezime = Prezime;
            original.Jmbg = Jmbg;
            original.Aktivnost = Aktivnost;
            original.Admin = Admin;
            return original;
        }
    }
    public enum Aktivnost { Aktivan = 0, NeAktivan = 1 };
    public enum Admin { Nije = 0, Jeste = 1 }
}
