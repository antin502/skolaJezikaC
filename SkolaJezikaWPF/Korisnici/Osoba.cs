﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Korisnici
{
    public class Osoba
    {
        public string Ime { get; set; }

        public string Prezime { get; set; }

        public string Jmbg { get; set; }
        public Aktivnost Aktivnost { get; set; } //0 aktivan, 1 neaktivan

        public Osoba()
        {
            this.Ime = "";
            this.Prezime = "";
            this.Jmbg = "";
            this.Aktivnost = Aktivnost.Aktivan;
        }

        public Osoba(string ime, string prezime, string jmbg,Aktivnost akt)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.Jmbg = jmbg;
            this.Aktivnost = akt;

        }
    }
}
