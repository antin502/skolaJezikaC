﻿using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF.Korisnici
{
    public class Ucenik :Osoba,ICloneable
    {
        public Ucenik():base(){}
        public Ucenik(string ime, string prezime, string jmbg, Aktivnost aktivan) : base(ime, prezime, jmbg,aktivan)
        {
        }

        public object Clone()
        {
            Ucenik original = new Ucenik();
            original.Ime = Ime;
            original.Prezime = Prezime;
            original.Jmbg = Jmbg;
            original.Aktivnost = Aktivnost;
            return original;
        }
        public override string ToString()
        {
            return Ime+" "+Prezime;
        }
    }
}
