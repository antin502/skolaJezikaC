﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class NastavnikDAO
    {
        public static void Create(Nastavnik n)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into nastavnik values(@ime,@prezime,@jmbg,@plata,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@ime", n.Ime));
                cmd.Parameters.Add(new SqlParameter("@prezime", n.Prezime));
                cmd.Parameters.Add(new SqlParameter("@jmbg", n.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@plata", n.Plata));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)n.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Nastavnik "+where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "nastavnici"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["nastavnici"].Rows)
                {
                    Nastavnik n = new Nastavnik();
                    n.Ime = (string)row["ime"];
                    n.Prezime = (string)row["prezime"];
                    n.Jmbg = (string)row["jmbg"];
                    n.Plata = Convert.ToDouble(row["plata"]);
                    n.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    Aplikacija.Instanca.Nastavnici.Add(n);
                    Console.WriteLine(n);

                }
            }
        }
        public static void Update(Nastavnik n)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update nastavnik set ime=@ime,prezime=@prezime,plata=@plata,aktivnost=@aktivnost where jmbg=@jmbg";
                cmd.Parameters.Add(new SqlParameter("@ime", n.Ime));
                cmd.Parameters.Add(new SqlParameter("@prezime", n.Prezime));
                cmd.Parameters.Add(new SqlParameter("@jmbg", n.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@plata", n.Plata));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)n.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }

    }
}
