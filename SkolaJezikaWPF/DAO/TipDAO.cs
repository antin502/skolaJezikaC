﻿using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class TipDAO
    {
        public static void Create(TipKursa t)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into tip values(@naziv,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@naziv", t.Naziv));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)t.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                    t.ID = Aplikacija.Instanca.getId("tip");
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from tip " + where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "tipovi"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["tipovi"].Rows)
                {
                    TipKursa t = new TipKursa();
                    t.ID= Convert.ToInt32(row["id"]);
                    t.Naziv = (string)row["naziv"];
                    t.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    Aplikacija.Instanca.Tipovi.Add(t);
                    Console.WriteLine(t.ID);

                }
            }
        }
        public static void Update(TipKursa t)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update tip set naziv=@naziv,aktivnost=@aktivnost where id=@id";
                cmd.Parameters.Add(new SqlParameter("@id", t.ID));
                cmd.Parameters.Add(new SqlParameter("@naziv", t.Naziv));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)t.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
    }
}
