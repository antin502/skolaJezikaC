﻿using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class UplataDAO
    {
        public static void Create(Uplata u)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into Uplata values(@jmbgUcenika,@OznakaKursa,@datum_uplate,@cena,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@jmbgUcenika", u.Uplatilac.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@OznakaKursa", u.Kurs.OznakaKursa));
                cmd.Parameters.Add(new SqlParameter("@datum_uplate", u.DatumUplate));
                cmd.Parameters.Add(new SqlParameter("@cena", u.Cena));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)u.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                    u.Id = Aplikacija.Instanca.getId("Uplata");
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from uplata " + where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "uplate"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["uplate"].Rows)
                {
                    Uplata u = new Uplata();
                    u.Id = Convert.ToInt32(row["id"]);
                    string jmbg = (string)(row["jmbgUcenika"]);
                    u.Uplatilac = Aplikacija.Instanca.PronadjiUcenika(jmbg);
                    int oznaka = Convert.ToInt32(row["OznakaKursa"]);
                    u.Kurs = Aplikacija.Instanca.PronadjiKurs(oznaka);
                    u.DatumUplate = Convert.ToDateTime(row["datum_uplate"]);
                    u.Cena = Convert.ToDouble(row["cena"]);
                    u.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    Aplikacija.Instanca.Uplate.Add(u);
                    Console.WriteLine(u);
                    
                }
            }
        }
        public static void Delete(Uplata u)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update uplata set aktivnost=@aktivnost where ID=@id";
                cmd.Parameters.Add(new SqlParameter("@id", u.Id));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)u.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }

    }
}
