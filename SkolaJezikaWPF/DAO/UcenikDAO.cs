﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class UcenikDAO
    {
        public static void Create(Ucenik u)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into Ucenik values(@ime,@prezime,@jmbg,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@ime", u.Ime));
                cmd.Parameters.Add(new SqlParameter("@prezime", u.Prezime));
                cmd.Parameters.Add(new SqlParameter("@jmbg", u.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)u.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Ucenik "+where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "ucenici"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["ucenici"].Rows)
                {
                    Ucenik u = new Ucenik();
                    u.Ime = (string)row["ime"];
                    u.Prezime = (string)row["prezime"];
                    u.Jmbg = (string)row["jmbg"];
                    u.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    Aplikacija.Instanca.Ucenici.Add(u);
                    Console.WriteLine(u);

                }
            }
        }
        public static void Update(Ucenik n)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update Ucenik set ime=@ime,prezime=@prezime,aktivnost=@aktivnost where jmbg=@jmbg";
                cmd.Parameters.Add(new SqlParameter("@ime", n.Ime));
                cmd.Parameters.Add(new SqlParameter("@prezime", n.Prezime));
                cmd.Parameters.Add(new SqlParameter("@jmbg", n.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)n.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }

    }
}
