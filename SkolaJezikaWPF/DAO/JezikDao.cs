﻿using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class JezikDao
    {
        public static void Create(Jezik j)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into jezik values(@naziv,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@naziv", j.Naziv));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)j.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                    j.Oznaka = Aplikacija.Instanca.getId("Jezik");
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Jezik "+where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "jezici"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["jezici"].Rows)
                {
                    Jezik j = new Jezik();
                    j.Oznaka = Convert.ToInt32(row["oznaka"]);
                    j.Naziv= (string)row["naziv"];
                    j.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    Aplikacija.Instanca.Jezici.Add(j);
                    Console.WriteLine(j);

                }
            }
        }
        public static void Update(Jezik j)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update jezik set naziv=@naziv,aktivnost=@aktivnost where oznaka=@oznaka";
                cmd.Parameters.Add(new SqlParameter("@oznaka", j.Oznaka));
                cmd.Parameters.Add(new SqlParameter("@naziv", j.Naziv));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)j.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
    }
}
