﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class KorisnikDAO
    {
        public static void Create(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into korisnik values(@kIme,@kLoz,@ime,@prezime,@jmbg,@aktivnost,@admin)";
                cmd.Parameters.Add(new SqlParameter("@kIme", k.Username));
                cmd.Parameters.Add(new SqlParameter("@kLoz", k.Password));
                cmd.Parameters.Add(new SqlParameter("@ime", k.Ime));
                cmd.Parameters.Add(new SqlParameter("@prezime", k.Prezime));
                cmd.Parameters.Add(new SqlParameter("@jmbg", k.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)k.Aktivnost));
                cmd.Parameters.Add(new SqlParameter("@admin", (int)k.Admin));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read()
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Korisnik";

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "korisnici"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["korisnici"].Rows)
                {
                    Korisnik k = new Korisnik();
                    k.Username = (string)row["kIme"];
                    k.Password = (string)row["kLoz"];
                    k.Ime = (string)row["ime"];
                    k.Prezime = (string)row["prezime"];
                    k.Jmbg = (string)row["jmbg"];
                    k.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    k.Admin = (Admin)Convert.ToInt16(row["admin"]);
                    Aplikacija.Instanca.Korisnici.Add(k);
                    Console.WriteLine(k);

                }
            }
        }
        public static Korisnik Login(string username,string password)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Korisnik where kIme=@username and kLoz=@password and aktivnost=0";
                cmd.Parameters.Add(new SqlParameter("@username", username));
                cmd.Parameters.Add(new SqlParameter("@password", password));
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Korisnik k = new Korisnik();
                    k.Username = (string)reader["kIme"];    
                    k.Password = (string)reader["kLoz"];
                    k.Ime = (string)reader["Ime"];
                    k.Prezime = (string)reader["Prezime"];
                    k.Jmbg = (string)reader["jmbg"];
                    k.Aktivnost = (Aktivnost)Convert.ToInt16(reader["aktivnost"]);
                    k.Admin = (Admin)Convert.ToInt16(reader["admin"]);
                    return k;
                }
                return null;

                //Console.WriteLine(ds.GetXml());
            }
        }
        public static void Update(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update korisnik set kLoz=@kLoz,ime=@ime,prezime=@prezime,jmbg=@jmbg,aktivnost=@aktivnost,admin=@admin where kIme=@kIme";
                cmd.Parameters.Add(new SqlParameter("@kIme", k.Username));
                cmd.Parameters.Add(new SqlParameter("@kLoz", k.Password));
                cmd.Parameters.Add(new SqlParameter("@ime", k.Ime));
                cmd.Parameters.Add(new SqlParameter("@prezime", k.Prezime));
                cmd.Parameters.Add(new SqlParameter("@jmbg", k.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)k.Aktivnost));
                cmd.Parameters.Add(new SqlParameter("@admin", (int)k.Admin));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }

    }
}
