﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class SkolaDAO
    {
        public static Skola Read()
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Skola";

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "skola"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["skola"].Rows)
                {
                    Skola sk = new Skola();
                    sk.Naziv = (string)row["naziv"];
                    sk.Adresa= (string)row["adresa"];
                    sk.Telefon = (string)row["telefon"];
                    sk.Email = (string)row["email"];
                    sk.InteretStranica = (string)row["sajt"];
                    sk.Pib = Convert.ToInt32(row["pib"]);
                    sk.Mbroj = (string)row["mb"];
                    sk.Zracun= (string)row["zr"];
                    Console.WriteLine(sk);
                    return sk;
                }
                return null;
            }
        }
        public static void Update(Skola sk)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update skola set naziv=@naziv,adresa=@adresa,telefon=@telefon,email=@email,sajt=@sajt,zr=@zr";
                cmd.Parameters.Add(new SqlParameter("@naziv", sk.Naziv));
                cmd.Parameters.Add(new SqlParameter("@adresa", sk.Adresa));
                cmd.Parameters.Add(new SqlParameter("@telefon", sk.Telefon));
                cmd.Parameters.Add(new SqlParameter("@email", sk.Email));
                cmd.Parameters.Add(new SqlParameter("@sajt", sk.InteretStranica));
                cmd.Parameters.Add(new SqlParameter("@zr", sk.Zracun));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }

    }
}
