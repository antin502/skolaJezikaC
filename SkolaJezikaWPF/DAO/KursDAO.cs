﻿using SkolaJezikaWPF.Korisnici;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class KursDAO
    {
        public static void Create(Kurs.Kurs k)
        {

            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into kurs values(@OznakaJezika,@OznakaTipa,@datum_pocetka,@datum_kraja,@cena,@jmbgNastavnika,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@OznakaJezika", k.StraniJezik.Oznaka));
                cmd.Parameters.Add(new SqlParameter("@OznakaTipa", k.NivoKursa.ID));
                cmd.Parameters.Add(new SqlParameter("@datum_pocetka", k.DatePocetak));
                cmd.Parameters.Add(new SqlParameter("@datum_kraja", k.DateKraj));
                cmd.Parameters.Add(new SqlParameter("@cena", k.Cena));
                cmd.Parameters.Add(new SqlParameter("@jmbgNastavnika", k.Predavac.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)k.Aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                    k.OznakaKursa = Aplikacija.Instanca.getId("Kurs");
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Kurs " + where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "kursevi"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["kursevi"].Rows)
                {
              //      "(@OznakaJezika,@OznakaTipa,@datum_pocetka,@datum_kraja,@cena,@jmbgNastavnika,@aktivnost)";
                    Kurs.Kurs k = new Kurs.Kurs();
                    k.OznakaKursa = Convert.ToInt32(row["id"]);
                    k.StraniJezik = Aplikacija.Instanca.PronadjiJezik(Convert.ToInt32(row["OznakaJezika"]));
                    k.NivoKursa = Aplikacija.Instanca.PronadjiTip(Convert.ToInt32(row["OznakaTipa"]));
                    k.DatePocetak = Convert.ToDateTime(row["datum_pocetka"]);
                    k.DateKraj = Convert.ToDateTime(row["datum_kraja"]);
                    k.Cena = Convert.ToDouble(row["cena"]);
                    string jmbg = (string)(row["jmbgNastavnika"]);
                    k.Predavac = Aplikacija.Instanca.PronadjiNastavnika(jmbg);
                    k.Aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    if(k.Predavac.Aktivnost==Aktivnost.NeAktivan || k.StraniJezik.Aktivnost==Aktivnost.NeAktivan || k.NivoKursa.Aktivnost == Aktivnost.NeAktivan)
                    {
                        k.Aktivnost = Aktivnost.NeAktivan;
                        Update(k);
                    }
                    Aplikacija.Instanca.Kursevi.Add(k);
                    Console.WriteLine(k);

                }
            }
        }
        public static void Update(Kurs.Kurs k)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update kurs set OznakaJezika=@OznakaJezika,OznakaTipa=@OznakaTipa,datum_pocetka=@datum_pocetka,datum_kraja=@datum_kraja,cena=@cena," +
                                                    "jmbgNastavnika=@jmbgNastavnika,aktivnost=@aktivnost where ID=@id";
                cmd.Parameters.Add(new SqlParameter("@OznakaJezika", k.StraniJezik.Oznaka));
                cmd.Parameters.Add(new SqlParameter("@OznakaTipa", k.NivoKursa.ID));
                cmd.Parameters.Add(new SqlParameter("@datum_pocetka", k.DatePocetak));
                cmd.Parameters.Add(new SqlParameter("@datum_kraja", k.DateKraj));
                cmd.Parameters.Add(new SqlParameter("@cena", k.Cena));
                cmd.Parameters.Add(new SqlParameter("@jmbgNastavnika", k.Predavac.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)k.Aktivnost));
                cmd.Parameters.Add(new SqlParameter("@id", k.OznakaKursa));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
    }
}
