﻿using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkolaJezikaWPF.DAO
{
    class UcenikNaKursuDAO
    {
        public static void Create(UcenikNaKurs u)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "insert into UcenikNaKursu values(@OznakaKursa,@jmbgUcenika,@aktivnost)";
                cmd.Parameters.Add(new SqlParameter("@jmbgUcenika", u.ucenik.Jmbg));
                cmd.Parameters.Add(new SqlParameter("@OznakaKursa", u.kurs.OznakaKursa));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)u.aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                    u.Id = Aplikacija.Instanca.getId("UcenikNaKursu");
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }
        public static void Read(string where)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from UcenikNaKursu " + where;

                SqlDataAdapter sqlDA = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sqlDA.Fill(ds, "uceniciNaKursu"); //ne mora da odgovara nazivu iz baze

                //Console.WriteLine(ds.GetXml());
                foreach (DataRow row in ds.Tables["uceniciNaKursu"].Rows)
                {
                    UcenikNaKurs u = new UcenikNaKurs();
                    u.Id = Convert.ToInt32(row["id"]);
                    string jmbg = (string)(row["jmbgUcenika"]);
                    u.ucenik= Aplikacija.Instanca.PronadjiUcenika(jmbg);
                    int oznaka = Convert.ToInt32(row["OznakaKursa"]);
                    u.kurs = Aplikacija.Instanca.PronadjiKurs(oznaka);
                    u.aktivnost = (Aktivnost)Convert.ToInt16(row["aktivnost"]);
                    Aplikacija.Instanca.UceniciNaKursu.Add(u);
                    Console.WriteLine(u);

                }
            }
        }
        public static void Delete(UcenikNaKurs u)
        {
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update UcenikNaKursu set aktivnost=@aktivnost where ID=@id";
                cmd.Parameters.Add(new SqlParameter("@id", u.Id));
                cmd.Parameters.Add(new SqlParameter("@aktivnost", (int)u.aktivnost));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message, "Error");
                }

            }
        }

    }
}
