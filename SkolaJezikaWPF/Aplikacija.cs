﻿using SkolaJezikaWPF.DAO;
using SkolaJezikaWPF.Korisnici;
using SkolaJezikaWPF.Kurs;
using SkolaJezikaWPF.Menadzeri;
using SkolaJezikaWPF.ModelSpajanja;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF
{
    class Aplikacija
    {
        public const string CONN_STR = @"data source=.\SQLEXPRESS;
                                        initial catalog = SkolaJezikaSF57;
                                        integrated security=true";

        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Nastavnik> Nastavnici { get; set; }
        public ObservableCollection<Ucenik> Ucenici { get; set; }
        public ObservableCollection<Jezik> Jezici { get; set; }
        public ObservableCollection<TipKursa> Tipovi { get; set; }
        public ObservableCollection<Kurs.Kurs> Kursevi { get; set; }
        public ObservableCollection<UcenikNaKurs> UceniciNaKursu { get; set; }
        public ObservableCollection<Uplata> Uplate { get; set; }
        private static Aplikacija instanca = new Aplikacija();
        public static Aplikacija Instanca
        {
            get { return instanca; }
        }
        private Aplikacija()
        {

        }
        public int getId(string table)
        {
            int retval = -1;
            using (SqlConnection conn = new SqlConnection(Aplikacija.CONN_STR))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select ident_current('" + table + "')";
                retval = Convert.ToInt32(cmd.ExecuteScalar());

            }
            return retval;
        }
        public void UcitajKorisnike()
        {
            Korisnici = new ObservableCollection<Korisnik>();
            KorisnikDAO.Read();
        }
        public Korisnik PronadjiKorisnikaPoUsername(string username)
        //Pretrazuje korisnike po username-u
        //kad ga pronadje vraca Korisnika kao parametar
        {
            foreach (Korisnik k in Korisnici)
            {
                if (k.Username == username)
                {
                    return k;
                }
            }
            return null;
        }
        public Korisnik PronadjiKorisnikaPoJMBG(string jmbg)
        {
            foreach (Korisnik k in Korisnici)
            {
                if (k.Jmbg == jmbg)
                {
                    return k;
                }
            }
            return null;
        }
        public void ucitajNastavnike(string where = "where aktivnost!=1")
        {
            Nastavnici = new ObservableCollection<Nastavnik>();
            NastavnikDAO.Read(where);
        }
        public void ucitajUcenike(string where = "where aktivnost!=1")
        {
            Ucenici = new ObservableCollection<Ucenik>();
            UcenikDAO.Read(where);
        }
        public void ucitajJezike(string where = "where aktivnost!=1")
        {
            Jezici = new ObservableCollection<Jezik>();
            JezikDao.Read(where);
        }
        public void ucitajTipove(string where = "where aktivnost!=1")
        {
            Tipovi = new ObservableCollection<TipKursa>();
            TipDAO.Read(where);
        }
        public void ucitajKurseve(string where = "where aktivnost!=1")
        {
            Kursevi = new ObservableCollection<Kurs.Kurs>();
            KursDAO.Read(where);
        }
        public void ucitajUcenikeNaKursu(string where = "where aktivnost!=1")
        {
            UceniciNaKursu = new ObservableCollection<UcenikNaKurs>();
            UcenikNaKursuDAO.Read(where);
        }
        public void ucitajUplate(string where = "where aktivnost!=1")
        {
            Uplate = new ObservableCollection<Uplata>();
            UplataDAO.Read(where);
        }
        public Nastavnik PronadjiNastavnika(string jmbg)
        {
            foreach (Nastavnik n in Nastavnici)
            {
                if (jmbg == n.Jmbg)
                {
                    return n;
                }
            }
            return null;
        }
        public Ucenik PronadjiUcenika(string jmbg)
        {
            foreach (Ucenik u in Ucenici)
            {
                if (u.Jmbg == jmbg)
                {
                    return u;
                }
            }
            return null;
        }
        public Jezik PronadjiJezik(int id)
        {
            foreach (Jezik j in Jezici)
            {
                if (j.Oznaka == id)
                {
                    return j;
                }
            }
            return null;
        }
        public TipKursa PronadjiTip(int id)
        {
            foreach (TipKursa t in Tipovi)
            {
                if (t.ID == id)
                {
                    return t;
                }
            }
            return null;
        }
        public Kurs.Kurs PronadjiKurs(int oznaka)
        {
            //u slucaju dodavanja proveri da li kombinacija vec postoji
            foreach (Kurs.Kurs k in Kursevi)
            {
                if (k.OznakaKursa == oznaka)
                {
                    return k;
                }
            }
            return null;
        }
        public UcenikNaKurs PronadjiUcenikaNaKursu(UcenikNaKurs ucenikNaKursu)
        {
            //u slucaju dodavanja proveri da li kombinacija vec postoji
            foreach (UcenikNaKurs u in UceniciNaKursu)
            {
                if (u.ucenik.Jmbg == ucenikNaKursu.ucenik.Jmbg && u.kurs.OznakaKursa == ucenikNaKursu.kurs.OznakaKursa)
                {
                    return u;
                }
            }
            return null;
        }
        public ObservableCollection<UcenikNaKurs> DeepCopyUcenikNaKursu()
        {
            ucitajUcenikeNaKursu();
            ObservableCollection<UcenikNaKurs> original = new ObservableCollection<UcenikNaKurs>();
            foreach (UcenikNaKurs u in UceniciNaKursu)
            {
                original.Add((UcenikNaKurs)u.Clone());
            }
            return original;
        }
    }
}
