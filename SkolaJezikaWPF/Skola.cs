﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkolaJezikaWPF
{
    class Skola
    {
        public string Naziv { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string InteretStranica { get; set; }
        public int Pib { get; set; }
        public string Mbroj { get; set; }
        public string Zracun { get; set; }

        public Skola()
        {
        }
        public Skola(string naziv, string adresa, string telefon, string email, string internetStranica,
                        int pib, string mBroj, string zRacun)
        {
            this.Naziv = naziv;
            this.Adresa = adresa;
            this.Telefon = telefon;
            this.Email = email;
            this.InteretStranica = internetStranica;
            this.Pib = pib;
            this.Mbroj = mBroj;
            this.Zracun = zRacun;
        }
    }
}
